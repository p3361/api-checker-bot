import datetime as dt
from uuid import uuid4
import random
import re
from urllib.request import urlopen
from sqlalchemy import create_engine, not_, and_
from sqlalchemy.exc import SQLAlchemyError
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import scoped_session, sessionmaker, relationship
from sqlalchemy import (
    func, Integer,
    Column, BIGINT, DateTime, Boolean, Float, String,
    ForeignKey, Text
)
from sqlalchemy.dialects.postgresql import UUID, JSONB

from app.config import SQLALCHEMY_DATABASE_URL

engine = create_engine(SQLALCHEMY_DATABASE_URL)
session_factory = sessionmaker(bind=engine)
Session = scoped_session(session_factory)
session = Session()

Base = declarative_base()


class User(Base):
    __tablename__ = "users"

    id = Column(BIGINT, primary_key=True, index=True)
    first_name = Column(String(256))
    username = Column(String(256))
    is_bot = Column(Boolean, default=False)
    last_name = Column(String(256))
    language_code = Column(String(8))
    is_active = Column(Boolean, default=True)
    created_at = Column(DateTime, default=dt.datetime.utcnow)

    links = relationship('UserWebLink', back_populates='user')

    @staticmethod
    def add_user(chat_id, first_name, username, is_bot, last_name, language_code):
        try:
            user = session.query(User).filter(
                User.id == chat_id
            ).first()
            if user:
                return user
            user = User(
                id=chat_id,
                first_name=first_name,
                username=username,
                is_bot=is_bot,
                last_name=last_name,
                language_code=language_code
            )
            session.add(user)
            session.commit()
            return user
        except SQLAlchemyError as e:
            session.rollback()
            print(e)


class WebLink(Base):
    __tablename__ = "web_links"

    id = Column(BIGINT, primary_key=True, autoincrement=True)
    link = Column(String(256), unique=True, index=True)
    is_active = Column(Boolean, default=True)
    created_at = Column(DateTime, default=dt.datetime.utcnow)

    users = relationship('UserWebLink', back_populates='link')
    statistics = relationship('WebLinkStatistic', back_populates='link')

    @staticmethod
    def add_new_link(link):
        try:
            web_link = session.query(WebLink).filter(
                WebLink.link == link
            ).first()
            if web_link:
                return
            web_link = WebLink(link=link)
            session.add(web_link)
            session.commit()
            return web_link
        except SQLAlchemyError as e:
            session.rollback()
            print(e)

    @staticmethod
    def get_my_links(chat_id):
        my_links = session.query(WebLink).filter(
            WebLink.is_active == True,
            WebLink.users.any(and_(
                UserWebLink.user_id == chat_id,
                UserWebLink.is_active == True
            ))
        ).all()
        return my_links

    @staticmethod
    def get_links():
        links = session.query(WebLink).filter(
            WebLink.is_active == True
        ).all()
        return links


class UserWebLink(Base):
    __tablename__ = "user_links"

    id = Column(BIGINT, primary_key=True, autoincrement=True)
    user_id = Column(BIGINT, ForeignKey("users.id"), index=True)
    link_id = Column(BIGINT, ForeignKey("web_links.id"), index=True)
    is_active = Column(Boolean, default=True)
    created_at = Column(DateTime, default=dt.datetime.utcnow)

    user = relationship('User', back_populates='links')
    link = relationship('WebLink', back_populates='users')

    @staticmethod
    def add_new_link(chat_id, link_id):
        try:
            user_link = UserWebLink(
                user_id=chat_id,
                link_id=link_id
            )
            session.add(user_link)
            session.commit()
            return user_link
        except SQLAlchemyError as e:
            session.rollback()
            print(e)


class WebLinkStatistic(Base):
    __tablename__ = "link_statistics"

    id = Column(BIGINT, primary_key=True, autoincrement=True)
    link_id = Column(BIGINT, ForeignKey("web_links.id"), index=True)
    status_code = Column(Integer)
    elapsed_time = Column(Float)
    response = Column(JSONB)
    created_at = Column(DateTime, default=dt.datetime.utcnow)

    link = relationship('WebLink', back_populates='statistics')

    @staticmethod
    def log_statistic(link_id, status_code, elapsed_time, response):
        try:
            s = WebLinkStatistic(
                link_id=link_id,
                status_code=status_code,
                elapsed_time=elapsed_time,
                response=response
            )
            session.add(s)
            session.commit()
            return s
        except SQLAlchemyError as e:
            session.rollback()
            print(e)


class Message(Base):
    __tablename__ = "messages"

    id = Column(BIGINT, primary_key=True, autoincrement=True)
    user_id = Column(BIGINT, index=True)
    message = Column(Text)
    created_at = Column(DateTime, default=dt.datetime.utcnow)

    @staticmethod
    def log_message(chat_id, message):
        try:
            new_message = Message(
                user_id=chat_id,
                message=message
            )
            session.add(new_message)
            session.commit()
            return new_message
        except SQLAlchemyError as e:
            session.rollback()
            print(e)

    @staticmethod
    def new_message(chat_id, message):
        Message.log_message(chat_id, message)
        messages = re.split("; |, |\*|\n|\t", message)
        i = 0
        for m in messages:
            if "http" in m or "www" in m:
                web_link = WebLink.add_new_link(m.strip())
                if web_link:
                    UserWebLink.add_new_link(chat_id, web_link.id)
                    i += 1
        return i


class Notification(Base):
    __tablename__ = "notifications"

    id = Column(BIGINT, primary_key=True, autoincrement=True)
    user_id = Column(BIGINT, index=True)
    link_id = Column(BIGINT)
    msg_id = Column(BIGINT)
    created_at = Column(DateTime, default=dt.datetime.utcnow)

    @staticmethod
    def send_notification(chat_id, link_id, msg_id):
        try:
            notification = Notification(
                user_id=chat_id,
                link_id=link_id,
                msg_id=msg_id
            )
            session.add(notification)
            session.commit()
            return notification
        except SQLAlchemyError as e:
            session.rollback()
            print(e)

    @staticmethod
    def get_lat_hour_user_notifications(chat_id):
        notifications = session.query(Notification).filter(
            Notification.user_id == chat_id,
            Notification.created_at >= dt.datetime.utcnow() - dt.timedelta(hours=1)
        ).count()
        return notifications >= 3
