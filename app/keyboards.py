from telebot import types
from emoji import emojize

# MESSAGES
INFO_TEXT = "Hi, I'm ApiChecker bot! {} \n\n" \
            "Add the URLs of your services and I'll check them. \n" \
            "If service doesn't respond to requests, I'll send you a notification. \n\n" \
            "Weekly reports coming soon. {}".format(
                emojize(":robot_face:", use_aliases=True),
                emojize(":smiling_face:", use_aliases=True)
            )


# BUTTONS
INFO_BTN = "{} Info".format(emojize(":information:", use_aliases=True))
MY_SERVICES_BTN = "{} My services".format(emojize(":globe_with_meridians:", use_aliases=True))

LIKE_GRADE_BUTTON = "{}".format(emojize(":thumbs_up:", use_aliases=True))
UNLIKE_GRADE_BUTTON = "{}".format(emojize(":thumbs_down:", use_aliases=True))


# KEYBOARDS
def default_keyboard():
    """Default keyboard."""
    keyboard = types.ReplyKeyboardMarkup(row_width=2, resize_keyboard=True)
    info_btn = types.KeyboardButton(INFO_BTN)
    tip_btn = types.KeyboardButton(MY_SERVICES_BTN)
    keyboard.row(info_btn, tip_btn)
    return keyboard
