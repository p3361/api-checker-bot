import telebot
from emoji import emojize

from app.config import BOT_TOKEN

from app.db import (
    User, Message, WebLink
)
from app.keyboards import (
    INFO_BTN, INFO_TEXT, default_keyboard,
    MY_SERVICES_BTN
)


bot = telebot.TeleBot(BOT_TOKEN)


@bot.message_handler(commands=["start"])
def start(message):
    chat_id = message.chat.id
    first_name = message.from_user.first_name
    username = message.from_user.username
    is_bot = message.from_user.is_bot
    last_name = message.from_user.last_name
    language_code = message.from_user.language_code

    User.add_user(chat_id, first_name, username, is_bot, last_name, language_code)

    keyboard = default_keyboard()
    text = INFO_TEXT
    bot.send_message(message.chat.id, text, parse_mode="Markdown", reply_markup=keyboard)

    bot.send_message(
        110571772,
        "{} New user {}".format(emojize(":fire:", use_aliases=True), username),
        parse_mode="Markdown",
        reply_markup=keyboard
    )


@bot.message_handler(regexp=INFO_BTN)
def info(message):
    keyboard = default_keyboard()
    text = INFO_TEXT
    bot.send_message(message.chat.id, text, parse_mode="Markdown", reply_markup=keyboard)


@bot.message_handler(regexp=MY_SERVICES_BTN)
def info(message):
    keyboard = default_keyboard()
    links = WebLink.get_my_links(message.chat.id)
    if links:
        text = ""
        for link in links:
            text += "{}\n".format(link.link)
        text += "{}".format(emojize(":robot_face:", use_aliases=True))
    else:
        text = "You haven't any links yet. {}".format(emojize(":smiling_face_with_sunglasses:", use_aliases=True))
    bot.send_message(message.chat.id, text, parse_mode="Markdown", reply_markup=keyboard)


@bot.message_handler(func=lambda message: True)
def text(message):
    keyboard = default_keyboard()
    msgs = Message.new_message(message.chat.id, message.text)
    if msgs == 0:
        text = "Sorry, an error has occurred {}".format(emojize(":crying_face:", use_aliases=True))
    elif msgs == 1:
        text = "Link added for tracking {}".format(emojize(":slightly_smiling_face:", use_aliases=True))
    else:
        text = "Links added for tracking {}".format(emojize(":rocket:", use_aliases=True))
    bot.send_message(message.chat.id, text, parse_mode="Markdown", reply_markup=keyboard)

    bot.send_message(
        110571772,
        "{} New message \n {}".format(emojize(":warning:", use_aliases=True), message.text),
        parse_mode="Markdown",
        reply_markup=keyboard
    )


if __name__ == "__main__":
    bot.infinity_polling()
