import requests
import time
import telebot
from emoji import emojize

from app.config import BOT_TOKEN
from app.db import WebLink, WebLinkStatistic, Notification
from app.keyboards import default_keyboard


bot = telebot.TeleBot(BOT_TOKEN)


def send_notification(link, message=None):
    if not message:
        message = "{} Please check {}".format(emojize(":exclamation:", use_aliases=True), link.link)
    users = link.users
    for user in users:
        notification_flag = Notification.get_lat_hour_user_notifications(user.user_id)
        if notification_flag:
            continue
        keyboard = default_keyboard()
        response = bot.send_message(user.user_id, message, parse_mode="Markdown", reply_markup=keyboard)
        Notification.send_notification(user.user_id, link.id, response.message_id)


def check_link(link):
    if not link:
        return
    try:
        N = 3
        err = 0
        for i in range(N):
            response = requests.get(link.link)
            status_code = response.status_code
            if status_code != 200:
                err += 1
            try:
                data = response.json()
            except:
                data = None
            elapsed_time = response.elapsed.microseconds / 1_000_000
            WebLinkStatistic.log_statistic(link.id, status_code, elapsed_time, data)
            time.sleep(0.1)
        if err < N:
            return True
    except Exception as e:
        print(e)


def main():
    while True:
        try:
            links = WebLink.get_links()
            for link in links:
                status = check_link(link)
                if not status:
                    send_notification(link)
        except Exception as e:
            print(e)
        time.sleep(60)


if __name__ == "__main__":
    main()
