from app.db import Base, engine

if __name__ == '__main__':
    """
    DROP SCHEMA public CASCADE;
    CREATE SCHEMA public;
    """
    Base.metadata.create_all(engine)
